## Acessar o site

- https://www.icloud.com/
- informar usuário e senha da Apple

## Acessar as configurações

![Configurações](/telainicial.png)

## Selecionar o dispositivo que deseja remover da conta

![Selecionar dispositivo](/dispositivos.png)

## Clicar o botão para remover o disposito

![Remover](/remover.png)

## Confirmar a operação

![Confirmar](/confirmar.png)